var SemanticUIColors = ["red", "orange", "yellow", "olive", "green", "teal", "blue", "violet", "purple", "pink", "brown"]

$(document).ready(function () {
    $('.dream-flip-toggle')
	.click(function () {
	    var c = $('.flip-container')

	    if (dreamBodyBgSwitchIndex === 0) {
		c.css('overflow', 'hidden')
		setBackground(dreamBody, dreamBodyBgSwitch[1])
		dreamBodyBgSwitchIndex = 1
	    } else {
		c.removeAttr('style')
		setBackground(dreamBody, dreamBodyBgSwitch[0])
		dreamBodyBgSwitchIndex = 0
	    }

	    c.toggleClass('flip-it')
	})

    var postList = $('.post-list')
    var pMaxHeight = $(window).height() - $('.ui.menu').outerHeight(true)

    if ($(window).width() > 974)
	postList.css('max-height', pMaxHeight)

    $('.ui.cards .image')
	.dimmer({
	    opacity: .4,
	    closable: false
	})
	.dimmer('show')

    $('.ui.accordion')
	.accordion()

    $('.ui.card')
	.css('cursor', "pointer")
	.click(function() {
	    window.location.href = $(this).attr('id');
	});

    $('.ui.card .image')
	.dimmer({
	    opacity: .2,
	    closable: false
	})
	.dimmer('show')

    setSemanticUIColor()
})

function randomInt(min, max) {
    min = Math.ceil(min)
    max = Math.floor(max)
    random = Math.floor(Math.random() * (max - min)) + min
    return random
}

function setSemanticUIColor() {
    var tagsParent = $('.dream-tags')
    tagsParent.children().map(function() {
	$(this).addClass(SemanticUIColors[randomInt(0, SemanticUIColors.length)])
    })
}
